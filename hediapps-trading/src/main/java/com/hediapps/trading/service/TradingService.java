package com.hediapps.trading.service;

import com.hediapps.trading.client.QuandlClient;
import com.hediapps.trading.config.ApplicationProperties;
import com.hediapps.trading.dto.DatasetResponseDto;
import com.hediapps.trading.dto.SearchResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TradingService {

    private final ApplicationProperties applicationProperties;
    private final QuandlClient quandlClient;

    public DatasetResponseDto getTimeSeries(String databaseCode, String datasetCode, int limit) {
        return quandlClient.getTimeSeries(databaseCode,
                datasetCode,
                limit,
                applicationProperties.getQuandl().getApiKey());
    }

    public SearchResultDto search(String query) {
        return quandlClient.find(query, applicationProperties.getQuandl().getApiKey());
    }
}
