package com.hediapps.trading.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "trading")
public class ApplicationProperties {

    private QuandlProperties quandl;
    private ExchangeRatesProperties exchangeRates;

    @Getter
    @Setter
    public static class QuandlProperties extends FeignClientProperties {
        private String apiKey;
    }

    @Getter
    @Setter
    public static class ExchangeRatesProperties extends FeignClientProperties {
    }

    @Getter
    @Setter
    private static class FeignClientProperties {
        private String urlPrefix;
    }
}
