package com.hediapps.trading.client;

import com.hediapps.trading.dto.CurrencyRateResultDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "QuandlClient", url = "${trading.exchange-rates.url-prefix}")
public interface CurrencyRateClient {
    @GetMapping("/latest")
    CurrencyRateResultDto latest(@RequestParam("base") String base);
}
