package com.hediapps.trading.client;

import java.time.LocalDate;

import com.hediapps.trading.dto.DatasetResponseDto;
import com.hediapps.trading.dto.SearchResultDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "QuandlClient", url = "${trading.quandl.url-prefix}")
public interface QuandlClient {

    @GetMapping("/datasets.json")
    SearchResultDto find(@RequestParam("query") String query, @RequestParam("api_key") String apiKey);

    @GetMapping("/datasets/{databaseCode}/{datasetCode}.json")
    DatasetResponseDto getTimeSeries(@PathVariable("databaseCode") String databaseCode,
                               @PathVariable("datasetCode") String datasetCode,
                               @RequestParam("limit") int limit,
                               @RequestParam("api_key") String apiKey);
}
