package com.hediapps.trading.endpoint;

import com.hediapps.trading.dto.DatasetResponseDto;
import com.hediapps.trading.dto.SearchResultDto;
import com.hediapps.trading.service.TradingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TradingController {
    private final TradingService tradingService;

    @GetMapping("/time-series")
    public DatasetResponseDto getTimeSeries(@RequestParam("databaseCode") String databaseCode,
                                            @RequestParam("datasetCode") String datasetCode,
                                            @RequestParam("limit") int limit) {
        return tradingService.getTimeSeries(databaseCode, datasetCode, limit);
    }

    @GetMapping("/search")
    public SearchResultDto search(@RequestParam("query") String query) {
        return tradingService.search(query);
    }
}
