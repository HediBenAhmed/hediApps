package com.hediapps.trading.dto;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

@Data
public class DatasetDataDto {

    private int id;
    private String name;
    private String description;

    @JsonProperty("newest_available_date")
    private LocalDate newestAvailableDate;
    @JsonProperty("oldest_available_date")
    private LocalDate oldestAvailableDate;

    @JsonProperty("column_names")
    private List<String> columnNames;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    private String frequency;

    private List<Data> data;
    private int limit;

    @JsonDeserialize(using = DataJsonDeserializer.class)
    @lombok.Data
    @Builder
    public static class Data {

        private LocalDate date;
        private double open;
        private double high;
        private double low;
        private double last;
        private int volume;
        private double turnover;
    }
}
