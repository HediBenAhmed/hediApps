package com.hediapps.trading.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CurrencyRateResultDto {

    private String base;
    private LocalDate date;

    private Rates rates;

    @Data
    public static class Rates {
        @JsonProperty("USD")
        private double usd;

        @JsonProperty("EUR")
        private double eur;
    }
}
