package com.hediapps.trading.dto;

import java.util.List;

import lombok.Data;

@Data
public class SearchResultDto {

    private List<DatasetDto> datasets;
}
