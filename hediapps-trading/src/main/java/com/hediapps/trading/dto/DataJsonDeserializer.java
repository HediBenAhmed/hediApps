package com.hediapps.trading.dto;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataJsonDeserializer extends JsonDeserializer<DatasetDataDto.Data> {
    @Override
    public DatasetDataDto.Data deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        return DatasetDataDto.Data.builder()
                .date(LocalDate.parse(node.get(0).asText()))
                .open(node.get(1).asDouble())
                .high(node.get(2).asDouble())
                .low(node.get(3).asDouble())
                .last(node.get(4).asDouble())
                .volume(node.get(5).asInt())
                .turnover(node.get(6).asDouble())
                .build();
    }
}
