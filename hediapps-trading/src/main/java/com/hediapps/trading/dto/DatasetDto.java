package com.hediapps.trading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DatasetDto {

    private int id;
    @JsonProperty("dataset_code")
    private String datasetCode;

    @JsonProperty("database_code")
    private String databaseCode;

    private String description;

    private String name;
}
