package com.hediapps.trading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DatasetResponseDto {

    @JsonProperty("dataset")
    private DatasetDataDto dataset;
}
